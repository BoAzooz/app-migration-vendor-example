package com.vendor.impl;

import com.atlassian.migration.app.AccessScope;
import com.atlassian.migration.app.AppCloudMigrationGateway;
import com.atlassian.migration.app.MigrationDetails;
import com.atlassian.migration.app.PaginatedMapping;
import com.vendor.MyPluginComponent;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;

public class MyPluginComponentImpl implements MyPluginComponent, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);
    private final AppCloudMigrationGateway gateway;

    public MyPluginComponentImpl(AppCloudMigrationGateway migrationGateway) {
        this.gateway = migrationGateway;
        this.gateway.registerListener(this);
    }

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    @Override
    public void onStartAppMigration(String transferId, MigrationDetails migrationDetails) {

        // Retrieving ID mappings from the migration.
        // A complete guide can be found at https://developer.atlassian.com/platform/app-migration/getting-started/mappings/
        try {
            log.info("Migration context summary: " + new ObjectMapper().writeValueAsString(migrationDetails));
            PaginatedMapping paginatedMapping = gateway.getPaginatedMapping(transferId, "identity:user", 5);
            while (paginatedMapping.next()) {
                Map<String, String> mappings = paginatedMapping.getMapping();
                log.info("mappings = {}", new ObjectMapper().writeValueAsString(mappings));
            }

            // If you know exactly the entities you are looking for a migration mapping, you can query them directly by IDs (up to 100 in a single request)
            Map<String, String> mappingById = gateway.getMappingById(transferId, "identity:user", Collections.singleton("email/admin@example.com"));
        } catch (IOException e) {
            log.error("Error retrieving migration mappings", e);
        }

        // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
        try {
            OutputStream firstDataStream = gateway.createAppData(transferId);
            // You can even upload big files in here
            firstDataStream.write("Your binary data goes here".getBytes());
            firstDataStream.close();

            // You can also apply labels to distinguish files or to add meta data to support your import process
            OutputStream secondDataStream = gateway.createAppData(transferId, "some-optional-label");
            secondDataStream.write("more bytes".getBytes());
            secondDataStream.close();
        } catch (IOException e) {
            log.error("Error uploading files to the cloud", e);
        }
    }

    @Override
    public String getCloudAppKey() {
        return "my-cloud-app-key";
    }

    @Override
    public String getServerAppKey() {
        return "my-server-app-key";
    }

    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_IDENTITY, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }

    @Override
    public void destroy() {
        log.info("Deregistering listener");
        gateway.deregisterListener(this);
    }
}
