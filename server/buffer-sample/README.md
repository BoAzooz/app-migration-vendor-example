# Buffer Sample

This example uses the `atlassian-app-cloud-migration-tracker` library to make your listener more resilient against changes in the OSGi context.

That means that your app will not crash if it starts before the Migration Assistant is exposing `AppCloudMigrationGateway`.

It also protects your app from changes on the context. That means that should the dependencies get restarted, the `atlassian-app-cloud-migration-tracker` the library will 
buffer changes and re-play them when the environment stabilised.

## Important pieces to reproduce this

- Import this dependency:
```xml
<dependency>
     <groupId>com.atlassian</groupId>
     <artifactId>atlassian-app-cloud-migration-tracker</artifactId>
     <version>1.21</version>
 </dependency>
``` 
- Add the package `com.atlassian.migration.app.*,` in your `<Import-Package>` instruction with `resolution:="optional"`
- Inject `CloudMigrationAccessor` in your code
- Register one or more migration listeners

## Fix of ClassDefNotFound Bug In Tracker Library
Fix version: (1.21)
### Bug:
When installing Jira/Confluence Migration Assistant after a plugin is already installed, the plugin will become disabled with `ClassDefNotFound` exception.

## Building

Go to server app project root folder and then go to server/buffer-sample folder, build the project with:

    mvn clean install

This will produce a P2-plugin jar.

License
========

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.

