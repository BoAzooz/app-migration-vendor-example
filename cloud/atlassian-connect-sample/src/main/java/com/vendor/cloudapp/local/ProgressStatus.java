package com.vendor.cloudapp.local;

public enum ProgressStatus {
    READY,
    IN_PROGRESS,
    SUCCESS,
    FAILED,
    SKIPPED,
    INCOMPLETE
}

