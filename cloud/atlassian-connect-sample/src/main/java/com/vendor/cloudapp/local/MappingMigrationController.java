package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@IgnoreJwt
@RestController
@RequestMapping("migration")
public class MappingMigrationController extends BaseController {

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> mapping() {
        return ResponseEntity.ok(call(getFullPath() + "/", HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/mapping/{transferId}/page", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getMappingsForTransferId(@PathVariable String transferId, @RequestParam String namespace, @RequestParam(required = false) String lastEntity, @RequestParam(required = false) String pageSize) {
        return ResponseEntity.ok(call(getFullPath() + "/mapping/" + transferId + "/page?" + paginationQueryParams(lastEntity, pageSize, namespace), HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/mapping/{transferId}/find", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Map> getMappingsForTransferId(@PathVariable String transferId, @RequestParam String namespace, @RequestBody Set<String> ids) {
        return ResponseEntity.ok(call(getFullPath() + "/mapping/" + transferId + "/find?namespace=" + namespace, HttpMethod.POST, ids, Map.class));
    }

    @RequestMapping(value = "/feedback/{transferId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Void> postFeedbackForTransferId(@PathVariable String transferId, @RequestBody CloudFeedback cloudFeedback) {
        return ResponseEntity.ok(call(getFullPath() + "/feedback/" + transferId, HttpMethod.POST, cloudFeedback, Void.class));
    }

    @RequestMapping(value = "/data/{fileId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getS3URLForFileId(@PathVariable String fileId) {
        return ResponseEntity.ok(call(getFullPath() + "/data/" + fileId, HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/data/{transferId}/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getAllFilesForTransferId(@PathVariable String transferId) {
        return ResponseEntity.ok(call(getFullPath() + "/data/" + transferId + "/all", HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/progress/{transferId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Void> postStatusForTransferId(@PathVariable String transferId, @RequestBody ProgressEndpointDto progressEndpointDto) {
        return ResponseEntity.ok(call(getFullPath() + "/progress/" + transferId, HttpMethod.POST, progressEndpointDto, Void.class));
    }

    private String paginationQueryParams(String lastEntity, String pageSize, String namespace) {
        String result = "";
        if (!StringUtils.isEmpty(lastEntity)) result += "&lastEntity=" + lastEntity;
        if (!StringUtils.isEmpty(pageSize)) result += "&pageSize=" + pageSize;
        if (!StringUtils.isEmpty(namespace)) result += "&namespace=" + namespace;
        return result.replaceFirst("&", "");
    }
}
