package com.vendor.cloudapp.local;

import java.util.Collections;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpStatusCodeException;

public class BaseController {

    public static final String ATLASSIAN_CONNECT_MIGRATION_URL = "/rest/atlassian-connect/1/migration";
    protected static final Logger logger = LoggerFactory.getLogger(BaseController.class);
    @Value("${atlassian.product}")
    public String product;
    @Value("${cloud.site.url}")
    private String cloudSiteUrl;
    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    protected String getFullPath() {
        if ("confluence".equals(product)) {
            return cloudSiteUrl + "/wiki" + ATLASSIAN_CONNECT_MIGRATION_URL;
        } else if ("jira".equals(product)) {
            return cloudSiteUrl + ATLASSIAN_CONNECT_MIGRATION_URL;
        } else {
            throw new IllegalArgumentException("Invalid product specified " + product);
        }
    }

    protected <R, B> R call(String url, HttpMethod method, B jsonBody, Class<R> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<B> request = new HttpEntity<>(jsonBody, headers);
        R response = null;
        try {
            response = atlassianHostRestClients.authenticatedAsAddon().exchange(url, method, request, responseType).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            logger.warn("Error Body:" + errorBody, e);
        }
        return response;
    }
}
