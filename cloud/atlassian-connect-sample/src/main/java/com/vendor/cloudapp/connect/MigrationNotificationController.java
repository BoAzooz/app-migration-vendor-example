package com.vendor.cloudapp.connect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("notification")
public class MigrationNotificationController {

    private static final Logger logger = LoggerFactory.getLogger(MigrationNotificationController.class);

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> event(@RequestBody MigrationEvent event) {
        logger.info("Received webhook event notification: " + event);

        if ("APP_DATA_UPLOADED".equals(event.getWebhookEventType())) {
            logger.info("s3Key to get url for uploaded data : " + event.getS3Key());
            logger.info("label of uploaded data: " + event.getLabel());
        }
        return ResponseEntity.ok().build();
    }
}
